#include "Point2d.h"
#include <math.h>
#include <iostream>

size_t Point2d::m_count=0;

Point2d::Point2d(double x, double y) :Point2d()
{
   m_array[0] = x;
   m_array[1] = y;
}

Point2d::Point2d()
{
   m_count++;
   m_array = new double[2]();
}

Point2d::Point2d(const Point2d & point) :Point2d()
{
   *this = point;
}

Point2d::Point2d(Point2d && point):Point2d()
{
   m_array = point.m_array;
   point.m_array = nullptr;
}

Point2d::~Point2d()
{
   delete[] m_array;
}

size_t Point2d::getCount()
{
   return m_count;
}

Point2d & Point2d::operator=(const Point2d & point)
{
   m_array[0] = point.m_array[0];
   m_array[1] = point.m_array[1];
   return *this;
}

Point2d & Point2d::operator=(Point2d && point)
{
   m_array = point.m_array;
   m_array = nullptr;
   return *this;
}

double Point2d::distance(const Point2d & point) const
{
   //return sqrt((m_x - point.m_x)*(m_x - point.m_x) + (m_y - point.m_y)*(m_y - point.m_y));
   return 0;
}

void Point2d::print() const
{
   std::cout << "x=" << m_array[0] << " y=" << m_array[1]<<" "<<m_array<<" "<<getCount()<<std::endl;
}

Point2d duplicate(const Point2d & point)
{
   //return Point2d(point.m_x,point.m_y);
   return Point2d();
}
