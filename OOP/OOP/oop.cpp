#include "Point2d.h"
#include <iostream>



int main()
{/*
   Point2d first(3.0, 4.0);
   Point2d x;
   first.print();
   Point2d second(std::move(first));
   second.print();

   x.print();
*/
   Point2d x1(3, 2);
   Point2d y = x1;

   x1.print();

   // Point2d third;                             
   // third = std::move(second);

   return 0;
}
