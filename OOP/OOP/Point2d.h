#pragma once
class Point2d
{
   double *m_array;
   static size_t m_count;
public:
   Point2d(double x, double y);
   Point2d();
   Point2d(const Point2d& point);
   Point2d(Point2d&& point);

   ~Point2d();

   static size_t getCount();

   Point2d& operator=(const Point2d& point);
   Point2d& operator=(Point2d&& point);

   double distance(const Point2d& point) const;
   void print() const;

   friend Point2d duplicate(const Point2d& point);

   

};

